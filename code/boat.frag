/* boat image */
uniform sampler2D tex;

/* boat position */
uniform float boatX;

/* wave shape */
uniform float amplitude;
uniform float frequency;
uniform float level;
uniform float phase;

/* boat size */
uniform float size;

/* appearance */
uniform vec4 stroke;
uniform vec4 fill;

const vec4 transparent = vec4(0.0);

void main(void) {
  vec2 p = gl_TexCoord[0].xy;
  float t = frequency * boatX + phase;
  float boatY = amplitude * sin(t) + level;
  float boatT = atan(amplitude * frequency * cos(t), 2.0);
  mat2 boatM = mat2(cos(boatT), sin(boatT), sin(boatT), -cos(boatT));
  vec2 q = boatM * (p - vec2(boatX, boatY));
  vec4 c = texture2D(tex, size * q + vec2(0.5));
  gl_FragColor = vec4(mix(stroke.rgb, fill.rgb, c.rgb / (c.a > 0.0 ? c.a : 1.0)), c.a);
}
