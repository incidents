#include <math.h>

#include "wave.h"
#include "wave.frag.c"
#include "wave.vert.c"

struct wave *wave_init(struct wave *wave) {
  if (! wave) { return 0; }
  if (! shader_init(&wave->shader, wave_vert, wave_frag)) { return 0; }
  shader_uniform(wave, amplitude);
  shader_uniform(wave, frequency);
  shader_uniform(wave, level);
  shader_uniform(wave, phase);
  shader_uniform(wave, width);
  shader_uniform(wave, blend);
  shader_uniform(wave, stroke);
  shader_uniform(wave, fill);
  wave->value.amplitude = 0;
  wave->value.frequency = 0;
  wave->value.level = 0;
  wave->value.phase = 0;
  wave->value.width = 0;
  wave->value.blend = 0;
  wave->value.stroke.v[0] = 0;
  wave->value.stroke.v[1] = 0;
  wave->value.stroke.v[2] = 0;
  wave->value.stroke.v[3] = 1;
  wave->value.fill.v[0] = 1;
  wave->value.fill.v[1] = 1;
  wave->value.fill.v[2] = 1;
  wave->value.fill.v[3] = 1;
  return wave;
}

void wave_use(struct wave *wave) {
  glUseProgram(wave->shader.program);
  shader_updatef(wave, amplitude);
  shader_updatef(wave, frequency);
  shader_updatef(wave, level);
  shader_updatef(wave, phase);
  shader_updatef(wave, width);
  shader_updatef(wave, blend);
  shader_updatef4(wave, stroke);
  shader_updatef4(wave, fill);
}
