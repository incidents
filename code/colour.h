#ifndef COLOUR_H
#define COLOUR_H 1

#include "vector.h"

extern const struct vec4 red, orange, green, blue, white, black;

#endif
