#include "colour.h"

const struct vec4 red    = {{ 231/255.0,  13/255.0, 100/255.0, 255/255.0 }};
const struct vec4 orange = {{ 255/255.0, 200/255.0, 103/255.0, 255/255.0 }};
const struct vec4 green  = {{ 153/255.0, 255/255.0,  53/255.0, 255/255.0 }};
const struct vec4 blue   = {{   3/255.0,  71/255.0, 148/255.0, 255/255.0 }};
const struct vec4 white  = {{ 255/255.0, 255/255.0, 255/255.0, 255/255.0 }};
const struct vec4 black  = {{   0/255.0,   0/255.0,   0/255.0, 255/255.0 }};
