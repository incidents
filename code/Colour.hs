module Colour (Colour, red, orange, green, blue, white, black, blend) where

-- with 3 elements [r,g,b]
type Colour = [Double]

-- base colour palette
red, orange, green, blue, white, black :: Colour
red    = map (/ 255) [ 231,  13, 100 ]
orange = map (/ 255) [ 255, 200, 103 ]
green  = map (/ 255) [ 153, 255,  53 ]
blue   = map (/ 255) [   3,  71, 148 ]
white  = map (/ 255) [ 255, 255, 255 ]
black  = map (/ 255) [   0,   0,   0 ]

-- linear mixing of colours
blend :: Double -> Colour -> Colour -> Colour
blend k = zipWith (\a b -> a * (1 - k) + k * b)
