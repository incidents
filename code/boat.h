#ifndef BOAT_H
#define BOAT_H 1

#include "shader.h"
#include "vector.h"

struct boat { struct shader shader;
  struct { GLint tex, boatX, amplitude, frequency, level, phase, size,             stroke, fill; } uniform;
  struct { float tex, boatX, amplitude, frequency, level, phase, size; struct vec4 stroke, fill; } value;
  GLuint tex;
};

struct boat *boat_init(struct boat *boat);
void boat_use(struct boat *boat);

#endif
