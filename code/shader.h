#ifndef SHADER_H
#define SHADER_H 1

#include <GL/glew.h>

//======================================================================
// generic shader data
struct shader {
  GLint linkStatus;
  GLhandleARB program;
  GLhandleARB fragment;
  GLhandleARB vertex;
  const GLcharARB *fragmentSource;
  const GLcharARB *vertexSource;
};

//======================================================================
// generic shader uniform location access macro
#define shader_uniform(self,name) \
  (self)->uniform.name = \
     glGetUniformLocationARB((self)->shader.program, #name)

//======================================================================
// generic shader uniform update access macro (integer)
#define shader_updatei(self,name) \
  glUniform1iARB((self)->uniform.name, (self)->value.name)

//======================================================================
// generic shader uniform update access macro (float)
#define shader_updatef(self,name) \
  glUniform1fARB((self)->uniform.name, (self)->value.name)

//======================================================================
// generic shader uniform update access macro (float4)
#define shader_updatef4(self,name) \
  glUniform4fARB((self)->uniform.name, (self)->value.name.v[0], (self)->value.name.v[1], (self)->value.name.v[2], (self)->value.name.v[3])

//======================================================================
// generic shader initialization
struct shader *shader_init(
  struct shader *shader, const char *vert, const char *frag
);

#endif
