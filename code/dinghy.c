/*
gcc \
  -std=c99 -Wall -Wextra -pedantic \
  -O3 -march=native -fopenmp \
  -o dinghy dinghy.c \
  -lglfw -lGL -lGLU -lGLEW -lm
*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#define RECORD
#undef  RECORD

#define P 10
#define M 16
#define N 12
#define B 11
#define D 3

typedef GLfloat R;

const int width = 1280, height = 720;
const R stiffness  = 50
      , repulsion  = 1
      , density    = 5
      , pressure   = 1
      , gravity    = 10
      , bouyancy   = 20
      , resistance =  0.996
      , dt         =  0.004
      , pi         =  3.141592653589793;

typedef struct {
  R R[P];
  R r[P];
  R m[P][M][N];
  R x[P][M][N][D];
  R v[P][M][N][D];
  R l[P][M][N][B][B];
  R n[P][M][N][D];
  R fog[P][M][N];
} S;

typedef struct {
  R m[P][M];
  R x[P][M][D];
  R f[P][M][N][D];
  R g[P][M][D];
  R x0[P][D];
  R m0[P];
} T;

typedef struct {
  GLuint vboX;
  GLuint vboN;
  GLuint vboF;
  GLuint vboC;
  GLuint vboI;
  GLuint vao;
  GLuint i[P][M][N][2][3];
  R c[P][4];
} G;

R rnd(void) {
  return rand() / (R) RAND_MAX;
}

void multmm(R *m, R *n, R *o) {
  R t[D][D];
  for (int i = 0; i < D; ++i) {
    for (int j = 0; j < D; ++j) {
      t[i][j] = 0;
      for (int k = 0; k < D; ++k) {
        t[i][j] += m[i * D + k] * n[k * D + j];
      }
    }
  }
  for (int i = 0; i < D; ++i) {
    for (int j = 0; j < D; ++j) {
      o[i * D + j] = t[i][j];
    }
  }
}

void multmv(R *m, R *v, R *o) {
  R t[D];
  for (int i = 0; i < D; ++i) {
    t[i] = 0;
    for (int j = 0; j < D; ++j) {
      t[i] += m[i * D + j] * v[j];
    }
  }
  for (int i = 0; i < D; ++i) {
    o[i] = t[i];
  }
}

void hsv2rgb(float h, float s, float v, float *rp, float *gp, float *bp) {
  float i, f, p, q, t, r, g, b;
  int ii;
  if (s == 0.0) { r = v; g = v; b = v; } else {
    h = h - floor(h);
    h = h * 6.0;
    i = floor(h); ii = (int) i;
    f = h - i;
    p = v*(1.0 - s);
    q = v*(1.0 - (s*f));
    t = v*(1.0 - (s*(1.0 - f)));
    switch(ii) {
      case 0:          r = v; g = t; b = p; break;
      case 1:          r = q; g = v; b = p; break;
      case 2:          r = p; g = v; b = t; break;
      case 3:          r = p; g = q; b = v; break;
      case 4:          r = t; g = p; b = v; break;
      case 5: default: r = v; g = p; b = q; break;
    }
  }
  *rp = r;
  *gp = g;
  *bp = b;
}

void initG(S *s, G *g) {
  glGenBuffers(1, &g->vboX);
  glGenBuffers(1, &g->vboN);
  glGenBuffers(1, &g->vboF);
  glGenBuffers(1, &g->vboC);
  glGenBuffers(1, &g->vboI);
  glGenVertexArrays(1, &g->vao);
  glBindVertexArray(g->vao);
  glBindBuffer(GL_ARRAY_BUFFER, g->vboX);
  glBufferData(GL_ARRAY_BUFFER, P * M * N * D * sizeof(R), 0, GL_STREAM_DRAW);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, g->vboN);
  glBufferData(GL_ARRAY_BUFFER, P * M * N * D * sizeof(R), 0, GL_STREAM_DRAW);
  glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(2);
  glBindBuffer(GL_ARRAY_BUFFER, g->vboF);
  glBufferData(GL_ARRAY_BUFFER, P * M * N * sizeof(R), 0, GL_STREAM_DRAW);
  glFogCoordPointer(GL_FLOAT, 0, 0);
  for (int p = 0; p < P; ++p) {
    hsv2rgb(0.38196601125010515 * p, 1, 1, &g->c[p][0], &g->c[p][1], &g->c[p][2]);
    g->c[p][3] = 1;
  }
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, g->vboI);
  for (int p = 0; p < P; ++p) {
    for (int m = 0; m < M; ++m) {
      int mm = (m + 1 + M) % M;
      for (int n = 0; n < N; ++n) {
        int nn = (n + 1 + N) % N;
        g->i[p][m][n][0][0] = (&s->x[p][m ][n ][0] - &s->x[0][0][0][0]) / D;
        g->i[p][m][n][0][1] = (&s->x[p][mm][n ][0] - &s->x[0][0][0][0]) / D;
        g->i[p][m][n][0][2] = (&s->x[p][m ][nn][0] - &s->x[0][0][0][0]) / D;
        g->i[p][m][n][1][0] = (&s->x[p][mm][nn][0] - &s->x[0][0][0][0]) / D;
        g->i[p][m][n][1][1] = (&s->x[p][m ][nn][0] - &s->x[0][0][0][0]) / D;
        g->i[p][m][n][1][2] = (&s->x[p][mm][n ][0] - &s->x[0][0][0][0]) / D;
      }
    }
  }
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, P * M * N * 2 * D * sizeof(GLuint), &g->i[0][0][0][0][0], GL_STATIC_DRAW);
}

void init(S *s) {
  memset(s, 0, sizeof(*s));
  for (int p = 0; p < P; ++p) {
    s->R[p] = 2 * rnd() + 2;
    s->r[p] = s->R[p] / (2 * rnd() + 2);
    R m1 = density * 2 * pi * s->R[p] * s->r[p] * s->r[p] / (M * N);
    R p1[D] = { 4 * (rnd() - 0.5), 4 * (rnd() - 0.5), -64 * rnd() };
    R u = rnd() * 2 * pi;
    R v = rnd() * 2 * pi;
    R w = rnd() * 2 * pi;
    R mu[D][D] = { { cos(u), sin(u), 0 }, { -sin(u), cos(u), 0 }, { 0, 0, 1 } };
    R mv[D][D] = { { cos(v), 0, sin(v) }, { 0, 1, 0 }, { -sin(v), 0, cos(v) } };
    R mw[D][D] = { { 1, 0, 0 }, { 0, cos(w), sin(w) }, { 0, -sin(w), cos(w) } };
    R o[D][D];
    multmm(&mu[0][0], &mv[0][0], &o[0][0]);
    multmm(&mw[0][0], &o[0][0], &o[0][0]);
    R a = 0;
    R b = 0;
    R c = 0;
    R oo[D][D] = { { 0, -c, b }, { c, 0, -a }, { -b, a, 0 } };
    for (int m = 0; m < M; ++m) {
      R a = m * 2 * pi / M;
      for (int n = 0; n < N; ++n) {
        R b = n * 2 * pi / N;
        s->m[p][m][n] = m1;
        s->x[p][m][n][0] = cos(a) * (s->R[p] + s->r[p] * cos(b));
        s->x[p][m][n][1] = sin(a) * (s->R[p] + s->r[p] * cos(b));
        s->x[p][m][n][2] =                     s->r[p] * sin(b);
        multmv(&oo[0][0], &s->x[p][m][n][0], &s->v[p][m][n][0]);
        multmv(&o[0][0], &s->x[p][m][n][0], &s->x[p][m][n][0]);
        multmv(&o[0][0], &s->v[p][m][n][0], &s->v[p][m][n][0]);
        for (int d = 0; d < D; ++d) {
          s->x[p][m][n][d] += p1[d];
        }
      }
    }
    for (int m = 0; m < M; ++m) {
      for (int n = 0; n < N; ++n) {
        for (int i = -B/2; i <= B/2; ++i) {
          for (int j = -B/2; j <= B/2; ++j) {
            int mm = (m + i + M) % M;
            int nn = (n + j + N) % N;
            if (i == 0 && j == 0) {
              nn = (n + N / 2) % N;
            }
            R l = 0;
            for (int d = 0; d < D; ++d) {
              R dl = s->x[p][m][n][d] - s->x[p][mm][nn][d];
              l += dl * dl;
            }
            s->l[p][m][n][i+B/2][j+B/2] = sqrt(l);
          }
        }
      }
    }
  }
}

void step(S *s, T *t) {
  /* zero temporary space */
  memset(t, 0, sizeof(*t));
  /* intra-mesh spring forces */
  #pragma omp parallel for
  for (int p = 0; p < P; ++p) {
    for (int m = 0; m < M; ++m) {
      for (int n = 0; n < N; ++n) {
        for (int i = -B/2; i <= B/2; ++i) {
          for (int j = -B/2; j <= B/2; ++j) {
            int mm = (m + i + M) % M;
            int nn = (n + j + N) % N;
            if (i == 0 && j == 0) {
              nn = (n + N / 2) % N;
            }
            R dx[D];
            R ldx = 0;
            for (int d = 0; d < D; ++d) {
              dx[d] = s->x[p][mm][nn][d] - s->x[p][m][n][d];
              ldx += dx[d] * dx[d];
            }
            if (!(ldx > 0)) { continue; }
            ldx = sqrt(ldx);
            R k = stiffness * (s->l[p][m][n][i+B/2][j+B/2] - ldx) / ldx;
            for (int d = 0; d < D; ++d) {
              t->f[p][m][n][d] -= dx[d] * k;
            }
          }
        }
      }
    }
  }
  /* mesh center circle */
  #pragma omp parallel for
  for (int p = 0; p < P; ++p) {
    for (int m = 0; m < M; ++m) {
      for (int n = 0; n < N; ++n) {
        for (int d = 0; d < D; ++d) {
          t->x[p][m][d] += s->x[p][m][n][d] * s->m[p][m][n];
          t->x0[p][d] += s->x[p][m][n][d] * s->m[p][m][n];
        }
        t->m[p][m] += s->m[p][m][n];
        t->m0[p] += s->m[p][m][n];
      }
      for (int d = 0; d < D; ++d) {
        t->x[p][m][d] /= t->m[p][m];
        t->x0[p][d] /= t->m0[p];
      }
    }
  }
  /* inflate */
  #pragma omp parallel for
  for (int p = 0; p < P; ++p) {
    for (int m = 0; m < M; ++m) {
      for (int n = 0; n < N; ++n) {
        R dx[D];
        R ldx = 0;
        for (int d = 0; d < D; ++d) {
          dx[d] = s->x[p][m][n][d] - t->x[p][m][d];
          ldx += dx[d] * dx[d];
        }
        ldx = sqrt(ldx);
        for (int d = 0; d < D; ++d) {
          t->f[p][m][n][d] += pressure * dx[d] / ldx;
        }
      }
    }
  }
  /* inter mesh forces */
  #pragma omp parallel for
  for (int p = 0; p < P; ++p) {
    for (int q = p + 1; q < P; ++q) {
      R l = 0;
      for (int d = 0; d < D; ++d) {
        l += (t->x0[p][d] - t->x0[q][d]) * (t->x0[p][d] - t->x0[q][d]);
      }
      l = sqrt(l);
      if (l < s->R[p] + s->r[p] + s->R[q] + s->r[q] + 0.01) {
        for (int mp = 0; mp < M; ++mp) {
          for (int mq = 0; mq < M; ++mq) {
            R dx[D];
            R ldx = 0;
            for (int d = 0; d < D; ++d) {
              dx[d] = t->x[p][mp][d] - t->x[q][mq][d];
              ldx += dx[d] * dx[d];
            }
            if (!(ldx > 0)) { continue; }
            ldx = sqrt(ldx);
            R k = pow(fmax(0.01, ldx - (s->r[p] + s->r[q])), -repulsion) / ldx;
            for (int d = 0; d < D; ++d) {
              R f = dx[d] * k;
              t->g[p][mp][d] += f;
              t->g[q][mq][d] -= f;
            }
          }
        }
      }
    }
  }
  /* normals */
  #pragma omp parallel for
  for (int p = 0; p < P; ++p) {
    for (int m = 0; m < M; ++m) {
      for (int n = 0; n < N; ++n) {
        R l = 0;
        for (int d = 0; d < D; ++d) {
          s->n[p][m][n][d] = s->x[p][m][n][d] - t->x[p][m][d];
          l += s->n[p][m][n][d] * s->n[p][m][n][d];
        }
        l = sqrt(l);
        for (int d = 0; d < D; ++d) {
          s->n[p][m][n][d] /= l;
        }
      }
    }
  }
  /* accumulate */
  #pragma omp parallel for
  for (int p = 0; p < P; ++p) {
    for (int m = 0; m < M; ++m) {
      for (int n = 0; n < N; ++n) {
        for (int d = 0; d < D; ++d) {
          R f = t->f[p][m][n][d] + t->g[p][m][d];
          f -= 0.2 * s->x[p][m][n][d];
          R a = f / s->m[p][m][n];
          if (d == D - 1) {
            a -= gravity + bouyancy * fmin(0, s->x[p][m][n][d]);
          }
          s->v[p][m][n][d] += a * dt;
          s->v[p][m][n][d] *= resistance;
          s->x[p][m][n][d] += s->v[p][m][n][d] * dt;
        }
        s->fog[p][m][n] = fmax(-s->x[p][m][n][D-1], 0);
      }
    }
  }
}

void display(GLFWwindow *window, S *s, G *g) {
  R x[D] = { 0, 0, 0 };
  for (int p = 0; p < P; ++p) {
    for (int m = 0; m < M; ++m) {
      for (int n = 0; n < N; ++n) {
        for (int d = 0; d < D; ++d) {
          x[d] += s->x[p][m][n][d];
        }
      }
    }
  }
  for (int d = 0; d < D; ++d) {
    x[d] /= P * M * N;
  }
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glBindBuffer(GL_ARRAY_BUFFER, g->vboX);
  glBufferSubData(GL_ARRAY_BUFFER, 0, P * M * N * D * sizeof(R), &s->x[0][0][0][0]);
  glBindBuffer(GL_ARRAY_BUFFER, g->vboN);
  glBufferSubData(GL_ARRAY_BUFFER, 0, P * M * N * D * sizeof(R), &s->n[0][0][0][0]);
  glBindBuffer(GL_ARRAY_BUFFER, g->vboF);
  glBufferSubData(GL_ARRAY_BUFFER, 0, P * M * N * 1 * sizeof(R), &s->fog[0][0][0]);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, g->vboI);

  // draw regular
  glEnable(GL_FOG);
  glEnableClientState(GL_FOG_COORD_ARRAY);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  GLfloat light_position[] = { 30.0, 10.0, 20.0, 1.0 };
  glLightfv(GL_LIGHT0, GL_POSITION, light_position);
  GLfloat spot_direction[] = { -3.0, -1.0, -2.0 };
  glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, spot_direction);
  gluLookAt(30, 100, 20, x[0], x[1], x[2], 0, 0, 1);
  glClear(GL_DEPTH_BUFFER_BIT);
  for (int p = 0; p < P; ++p) {
    glMaterialfv(GL_FRONT, GL_SPECULAR, &g->c[p][0]);
    glMaterialfv(GL_FRONT, GL_AMBIENT, &g->c[p][0]);
    glDrawElements(GL_TRIANGLES, M * N * 2 * 3, GL_UNSIGNED_INT, ((char *)0) + p * M * N * 2 * 3 * sizeof(GLuint));
  }
  glDisableClientState(GL_FOG_COORD_ARRAY);
  glDisable(GL_FOG);

  // draw reflection
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  GLfloat light_position2[] = { 30.0, 10.0, 20.0, 1.0 };
  glLightfv(GL_LIGHT0, GL_POSITION, light_position2);
  GLfloat spot_direction2[] = { -3.0, -1.0, -2.0 };
  glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, spot_direction2);
  gluLookAt(30, 100, 20, x[0], x[1], x[2], 0, 0, 1);
  glScalef(1, 1, -1);
  GLdouble clip[4] = { 0, 0, 1, 0 };
  glClipPlane(GL_CLIP_PLANE0, clip);
  glEnable(GL_CLIP_PLANE0);
  for (int p = 0; p < P; ++p) {
    glMaterialfv(GL_FRONT, GL_SPECULAR, &g->c[p][0]);
    glMaterialfv(GL_FRONT, GL_AMBIENT, &g->c[p][0]);
    glDrawElements(GL_TRIANGLES, M * N * 2 * 3, GL_UNSIGNED_INT, ((char *)0) + p * M * N * 2 * 3 * sizeof(GLuint));
  }
  glDisable(GL_CLIP_PLANE0);

  // draw surface
  glDisable(GL_LIGHTING);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_TEXTURE_2D);
  glBegin(GL_QUADS);
  glColor4f(0.1, 0.1, 0.2, 0.75);
  glNormal3f(0,0,1);
  glTexCoord2f(-250,-250);
  glVertex3f(-500,-500,0);
  glTexCoord2f(-250, 250);
  glVertex3f(-500, 500,0);
  glTexCoord2f( 250, 250);
  glVertex3f( 500, 500,0);
  glTexCoord2f( 250,-250);
  glVertex3f( 500,-500,0);
  glEnd();
  glDisable(GL_TEXTURE_2D);
  glDisable(GL_BLEND);
  glEnable(GL_LIGHTING);
  glfwSwapBuffers(window);
}

#ifdef RECORD
void capture(unsigned char *b) {
  glReadPixels(0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE, b);
  printf("P6\n%d %d\n255\n", width, height);
  fflush(stdout);
  fwrite(b, width * height * 3, 1, stdout);
  fflush(stdout);
}
#endif

int main(int argc, char **argv) {
  (void) argc;
  (void) argv;
  S s;
  T t;
  G g;
#ifdef RECORD
  unsigned char b[width * height * 3];
#endif
  srand(time(0));
  glfwInit();
  glfwWindowHint(GLFW_SAMPLES, 64);
  GLFWwindow *window = glfwCreateWindow(width, height, "dinghy", 0, 0);
  glfwMakeContextCurrent(window);
  glewExperimental = GL_TRUE;
  glewInit();
  glGetError(); // discard error from GLEW
  glMatrixMode(GL_PROJECTION);
  gluPerspective(20,  width * 1.0 / height,  10,  200);
  glEnable(GL_DEPTH_TEST);
  glShadeModel(GL_SMOOTH);
  glEnable(GL_LIGHTING);
  GLfloat mat_shininess[] = { 10.0 };
  glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
  glLightf(GL_LIGHT0, GL_SPOT_CUTOFF, 60.0);
  glEnable(GL_LIGHT0);
  glClearColor(0.1, 0.1, 0.2, 1.0);
  glFogi(GL_FOG_COORD_SRC, GL_FOG_COORD);
  glFogf(GL_FOG_DENSITY, 0.05);
  GLfloat fog[4] = { 0.1, 0.1, 0.2, 1.0 };
  glFogfv(GL_FOG_COLOR, fog);
  GLuint tex;
  glGenTextures(1, &tex);
  glBindTexture(GL_TEXTURE_2D, tex);
  GLfloat img[2][2][3] = { { { 1, 1, 1 }, { 0, 0 ,0 } }, { { 0, 0, 0 }, { 1, 1, 1 } } };
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 2, 2, 0, GL_RGB, GL_FLOAT, img);
  glGenerateMipmap(GL_TEXTURE_2D);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  initG(&s, &g);
  int frame = 0;
  while (! glfwWindowShouldClose(window)) {
    if (frame % (8 * 25) == 0) { init(&s); }
    if (frame++ == (2 * 60 + 38) * 25) { exit(0); }
    for (int i = 0; i < 20; ++i) {
      step(&s, &t);
    }
    display(window, &s, &g);
#ifdef RECORD
    capture(&b[0]);
#endif
    glfwPollEvents();
  }
  return 0;
}
