{-# LANGUAGE NoMonomorphismRestriction #-}

import Control.Monad (forM_, when)
import Graphics.Rendering.Cairo

import Colour

width  = 1920
height = 1080
zoom = height / 576
weight =    4 * zoom
frames = 4425
peak   = 2495 / frames
step   = 2734 -- frames `gcd` step == 1

boatX0 = 10 * zoom
waveSpeed = 2 * pi * frames / 50
waveZoom = 3 ** (1 / 7)

filename f = (reverse .  take 6 . (++ repeat '0') . reverse . show $ f) ++ ".png"

darkRed  = blend (1/3) red black
darkBlue = blend (2/3) blue black

renderFrame image f = renderWith image $ do
  let _boatColour@[boatR,boatG,boatB] = red
      skyColour@[skyR,skyG,skyB]
        | f < 0.5   = blend (f * 2    ) orange  darkRed
        | otherwise = blend (f * 2 - 1) darkRed darkBlue
      seaColours = reverse $ map ($ blue)
        [ blend (1/4) black . blend (1/4) green
        , blend (2/4) black . blend (2/4) green
        , blend (3/4) black . blend (3/4) green
        , blend (5/6) skyColour
        , blend (4/6) skyColour
        , blend (3/6) skyColour
        , blend (2/6) skyColour
        , blend (1/6) skyColour
        ]
      (earlySea, lateSea) = splitAt 5 ([-9,-7 ..] `zip` seaColours)
      waveSeparation = height * magic / 4
      vanishingPoint = waveSeparation * waveZoom ** 7
      magic = cos (pi * (f - 0.5))
      --magic = let x = (2 * f - 1) * pi / 3 in (1 - signum x * (1 - 1 / cos x)) / 2
      seaLevel = (height + 2 * weight) * (1 - magic / 2) - weight
      boatX = (width + 2 * boatX0) * (1 - cos (pi * f)) / 2 - boatX0
      boatY = waveLevel 0 + waveY 0 boatX
      boatSize = sin (pi * f) ^ 2
      waveSize = sin (pi * f)
      waveHeight bias = (max 0 $ height / 4 * (1 - ((f - peak) / (1 - peak))^2)) * waveZoom ** bias
      waveFreq = 4 / height
      waveY bias x = vanishingPoint * (waveZoom ** bias - 1) + waveSize * waveHeight bias * cos ((x - (width / 2)) * waveFreq * waveZoom ** negate bias + f * waveSpeed)
      waveLevel bias = seaLevel - 0.5 * waveY bias boatX + boatSize * 100 * zoom
      boatAngle = atan2 (waveY 0 (boatX + 1) - waveY 0 (boatX - 1)) 2
      drawWave (bias, [seaR, seaG, seaB]) = do
        setSourceRGB seaR seaG seaB
        moveTo (- 2 * weight) (waveLevel bias + waveY bias (- 2 * weight))
        forM_ [-weight .. width + weight] $ \x -> do
          let y = waveLevel bias + waveY bias x
          lineTo x y
        lineTo (width + 2 * weight) (waveLevel bias + waveY bias (width + 2 * weight))
        lineTo (width + 2 * weight) (height + 2 * weight)
        lineTo (      - 2 * weight) (height + 2 * weight)
        lineTo (      - 2 * weight) (waveLevel bias + waveY bias (      - 2 * weight))
        closePath
        fillPreserve
        setSourceRGB 0 0 0
        stroke
  setLineWidth (2 * zoom)
  setSourceRGB skyR skyG skyB
  paint
  forM_ earlySea drawWave
  when (boatSize > 0) $ do
    save
    setSourceRGB boatR boatG boatB
    translate boatX boatY
    rotate boatAngle
    scale (boatSize * zoom) (boatSize * zoom)
    moveTo (-180) (-50)
    forM_ [(-20,-50),(-20,-270),(140,-150),(-20,-70),(-20,-50),(200,-50),(120,30),(-120,30),(-180,-50)] $ \(x,y) -> do
      lineTo x y
    closePath
    fillPreserve
    restore
    setSourceRGB 0 0 0
    stroke
  forM_ lateSea drawWave

main = do
  image <- createImageSurface FormatARGB32 width height
  forM_ [ 0 .. frames - 1 ] $ \f -> do
    let g = (f * step) `mod` frames
    renderFrame image (fromIntegral g / frames)
    surfaceWriteToPNG image (filename g)
