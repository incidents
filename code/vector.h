#ifndef VECTOR_H
#define VECTOR_H 1

struct vec4 {
  float v[4];
};

void mix(const struct vec4 *x, const struct vec4 *y, float t, struct vec4 *o);

#endif
