#include <math.h>
#include <stdlib.h>

#include <cairo/cairo.h>

#include "boat.h"
#include "boat.frag.c"
#include "boat.vert.c"

struct boat *boat_init(struct boat *boat) {
  if (! boat) { return 0; }
  if (! shader_init(&boat->shader, boat_vert, boat_frag)) { return 0; }
  shader_uniform(boat, tex);
  shader_uniform(boat, boatX);
  shader_uniform(boat, amplitude);
  shader_uniform(boat, frequency);
  shader_uniform(boat, level);
  shader_uniform(boat, phase);
  shader_uniform(boat, size);
  shader_uniform(boat, stroke);
  shader_uniform(boat, fill);
  boat->value.tex = 0;
  boat->value.boatX = 0;
  boat->value.amplitude = 0;
  boat->value.frequency = 0;
  boat->value.level = 0;
  boat->value.phase = 0;
  boat->value.size = 1;
  boat->value.stroke.v[0] = 0;
  boat->value.stroke.v[1] = 0;
  boat->value.stroke.v[2] = 0;
  boat->value.stroke.v[3] = 1;
  boat->value.fill.v[0] = 1;
  boat->value.fill.v[1] = 1;
  boat->value.fill.v[2] = 1;
  boat->value.fill.v[3] = 1;
  glGenTextures(1, &boat->tex);
  glBindTexture(GL_TEXTURE_2D, boat->tex);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  for (int size = 4096, level = 0; size > 0; size >>= 1, level += 1) {
    cairo_surface_t *surface = cairo_image_surface_create (CAIRO_FORMAT_ARGB32, size, size);
    cairo_t *cr = cairo_create(surface);
    cairo_set_source_rgba(cr, 0, 0, 0, 0);
    cairo_paint(cr);
    if (size > 16) {
      cairo_save(cr);
      cairo_translate(cr, size/2.0, size/2.0);
      cairo_scale(cr, size/128.0, size/128.0);
  #define M(x,y) cairo_move_to(cr, x, y);
  #define L(x,y) cairo_line_to(cr, x, y);
  #define Z cairo_close_path(cr);
      M(-18,-5)L(-2,-5)L(-2,-27)L(14,-15)L(-2,-7)L(-2,-5)L(20,-5)L(12,3)L(-12,3)L(-18,-5)Z
  #undef M
  #undef L
  #undef Z
      cairo_restore(cr);
      cairo_set_line_width(cr, 4);
      cairo_set_source_rgba(cr, 1, 1, 1, 1);
      cairo_fill_preserve(cr);
      cairo_set_source_rgba(cr, 0, 0, 0, 1);
      cairo_stroke(cr);
    }
    cairo_destroy(cr);
    cairo_surface_flush(surface);
    unsigned char *pixels = cairo_image_surface_get_data(surface);
    int stride = cairo_image_surface_get_stride(surface);
    if (stride != size * 4) {
      exit(42);
    }
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glTexImage2D(GL_TEXTURE_2D, level, GL_RGBA, size, size, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
    cairo_surface_destroy(surface);
  }
  glBindTexture(GL_TEXTURE_2D, 0);
  return boat;
}

void boat_use(struct boat *boat) {
  if (boat) {
    glUseProgram(boat->shader.program);
    glBindTexture(GL_TEXTURE_2D, boat->tex);
    shader_updatei(boat, tex);
    shader_updatef(boat, boatX);
    shader_updatef(boat, amplitude);
    shader_updatef(boat, frequency);
    shader_updatef(boat, level);
    shader_updatef(boat, phase);
    shader_updatef(boat, size);
    shader_updatef4(boat, stroke);
    shader_updatef4(boat, fill);
  } else {
    glBindTexture(GL_TEXTURE_2D, 0);
    glUseProgram(0);
  }
}
