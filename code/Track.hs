module Track (titles, durations) where

-- track titles
titles :: [String]
titles =
  [ "nightboat"
  , "semaphore"
  , "prepare to be boarded"
  , "no quarter"
  , "sonar"
  , "subterfuge"
  , "bulkhead collapse"
  , "man overboard"
  , "inflatable"
  , "aeronautics"
  , "engine failure"
  , "freefall"
  , "cloctopus"
  , "fisheye"
  , "coda"
  , "bubblicious"
  ]

-- track durations in frames
durations :: [Double]
durations = []
