// for popen, pclose
#define _POSIX_C_SOURCE 2

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <GL/glew.h>
#include <GL/glut.h>

#include <jack/jack.h>

#include "boat.h"
#include "colour.h"
#include "vector.h"
#include "wave.h"

const double pi = 3.141592653589793;

const double waveSpeed = 556.0618996853933; // 2 * pi * 4425 / 50 / 3;
const double waveZoom = 1.272019649514069;//1.189207115002721; // 1.4142135623730951; // sqrt(2);sqrt $ (sqrt 5 + 1) / 2


struct vec4 darkRed, darkBlue;

struct {
  struct boat boat;
  struct wave wave;
  int width, height, winwidth, winheight, frame, frames, record, ready, bufsize;
  char *buffer;
  double time;
  jack_client_t *jack;
  FILE *eca;
} data;

void keyboard(unsigned char c, int x, int y) {
  if (! data.record) {
    exit(0);
  }
}

void timer(int t) {
  glutTimerFunc(data.record ? 1 : 10, timer, t + 1);
  glutPostRedisplay();
}

void reshape(int w, int h) {
  data.winwidth = w;
  data.winheight = h;
  data.ready = 1;
}

void update(void) {
  jack_position_t pos;
  /*jack_transport_state_t s = */ jack_transport_query(data.jack, &pos);
  double phase = pos.frame * 25.0 / pos.frame_rate / data.frames;
  data.time = phase - floor(phase);
}

void display(void) {
  if (data.ready) {
    update();
  }
  {
    // view
    float x = data.width * 0.5 / data.height;
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(-x, x, -0.5, 0.5);
    glViewport(0, 0, data.width, data.height);
    // background
    struct vec4 sky;
    if (data.time < 0.5) {
      mix(&orange, &darkRed, data.time * 2, &sky);
    } else {
      mix(&darkRed, &darkBlue, data.time * 2 - 1, &sky);
    }
    glClearColor(sky.v[0], sky.v[1], sky.v[2], sky.v[3]);
    glClear(GL_COLOR_BUFFER_BIT);
    // perspective
    int boatBias = -8;
    int viewBias =  4;
    double frequency = data.height * pi / data.width * pow(waveZoom, boatBias);
    double amplitude = (1 - cos(2 * pi * data.time)) / 2;
    double z = 1;//pow(8, amplitude);
    double boatFrequency = frequency * pow(waveZoom, -boatBias);
    double viewFrequency = frequency * pow(waveZoom, -viewBias);
    double boatAmplitude = amplitude / boatFrequency;
    double viewAmplitude = amplitude / viewFrequency;
    double boatX = data.width * -0.5 * cos(pi * data.time) / data.height;
    double viewX = 0;
    double boatPhase = data.time * waveSpeed;
    double viewPhase = data.time * waveSpeed;
    double boatY = boatAmplitude * (sin(boatX * boatFrequency + boatPhase) - 0.5);
    double viewY = boatY + viewAmplitude;
    double viewT = -atan2(viewFrequency * viewAmplitude * cos(viewX * viewFrequency + viewPhase), 16);
    double k = - (viewY + boatY) / (boatY - pow(waveZoom, viewBias - boatBias) * boatY);
    double tc = cos(viewT);
    double ts = sin(viewT);
    double dx = boatX;
    double dy = boatY;
    double y0 = -0.5 - dy;
    double y1 =  0.5 - dy;
    double tx00 = z * ( tc * (-x - dx) + ts * y0) + dx;
    double ty00 = z * (-ts * (-x - dx) + tc * y0) + dy + 1.0/6;
    double tx10 = z * ( tc * (-x - dx) + ts * y1) + dx;
    double ty10 = z * (-ts * (-x - dx) + tc * y1) + dy + 1.0/6;
    double tx01 = z * ( tc * ( x - dx) + ts * y0) + dx;
    double ty01 = z * (-ts * ( x - dx) + tc * y0) + dy + 1.0/6;
    double tx11 = z * ( tc * ( x - dx) + ts * y1) + dx;
    double ty11 = z * (-ts * ( x - dx) + tc * y1) + dy + 1.0/6;
    // distant waves
    for (int bias = -19; bias < boatBias; bias += 2) {
      data.wave.value.frequency = frequency * pow(waveZoom, -bias) / z;
      data.wave.value.amplitude = amplitude / data.wave.value.frequency;
      data.wave.value.level = (-pow(waveZoom, bias - boatBias) * boatY + boatY) * k - boatY;
      data.wave.value.phase = data.time * waveSpeed;
      data.wave.value.width = z * fmax(1, pow(waveZoom, bias)) / data.height;
      data.wave.value.blend = data.height / 2.0 / z;
      mix(&sky, &blue,  1 - (boatBias - bias) / 12.0, &data.wave.value.fill);
      data.wave.value.fill.v[3] = 0.25;
      mix(&sky, &white, 0.5 - (boatBias - bias) / 24.0, &data.wave.value.stroke);
      wave_use(&data.wave);
      glBegin(GL_QUADS); {
        glTexCoord2f(tx00, ty00); glVertex2f(-x, -0.5);
        glTexCoord2f(tx01, ty01); glVertex2f( x, -0.5);
        glTexCoord2f(tx11, ty11); glVertex2f( x,  0.5);
        glTexCoord2f(tx10, ty10); glVertex2f(-x,  0.5);
      } glEnd();
      glUseProgram(0);
    }
    // boat
    data.boat.value.boatX = boatX;
    data.boat.value.frequency = boatFrequency;
    data.boat.value.amplitude = boatAmplitude;
    data.boat.value.level = -boatY;
    data.boat.value.phase = boatPhase;
    data.boat.value.size = (0.33333 / (1 - fabs(boatX / x)) - 0.0625) / z;
    mix(&sky, &red,   1, &data.boat.value.fill);
    mix(&sky, &white, 0.5, &data.boat.value.stroke);
    boat_use(&data.boat);
    glBegin(GL_QUADS); {
      glTexCoord2f(tx00, ty00); glVertex2f(-x, -0.5);
      glTexCoord2f(tx01, ty01); glVertex2f( x, -0.5);
      glTexCoord2f(tx11, ty11); glVertex2f( x,  0.5);
      glTexCoord2f(tx10, ty10); glVertex2f(-x,  0.5);
    } glEnd();
    boat_use(0);
    // near waves
    for (int bias = boatBias + 1; bias < 4; bias += 2) {
      data.wave.value.frequency = frequency * pow(waveZoom, -bias) / z;
      data.wave.value.amplitude = amplitude / data.wave.value.frequency;
      data.wave.value.level = (-pow(waveZoom, bias - boatBias) * boatY + boatY) * k - boatY;
      data.wave.value.phase = data.time * waveSpeed;
      data.wave.value.width = z * fmax(1, pow(waveZoom, bias)) / data.height;
      data.wave.value.blend = data.height / 2.0 / z;
      struct vec4 tmp;
      float b = (bias - boatBias) / 12.0;
      mix(&blue, &green, b, &tmp);
      mix(&tmp, &black, b, &data.wave.value.fill);
      data.wave.value.fill.v[3] = 0.25;
      mix(&sky, &white, 0.5 - (boatBias - bias) / 24.0, &data.wave.value.stroke);
      wave_use(&data.wave);
      glBegin(GL_QUADS); {
        glTexCoord2f(tx00, ty00); glVertex2f(-x, -0.5);
        glTexCoord2f(tx01, ty01); glVertex2f( x, -0.5);
        glTexCoord2f(tx11, ty11); glVertex2f( x,  0.5);
        glTexCoord2f(tx10, ty10); glVertex2f(-x,  0.5);
      } glEnd();
      glUseProgram(0);
    }
  }
  glutSwapBuffers();
  { // recording
    if (data.ready && data.record) {
      if (data.frame == data.frames) {
        exit(0);
      }
      fprintf(stdout, "P6\n%d %d 255\n", data.width, data.height);
      glPixelStorei(GL_PACK_ALIGNMENT, 1);
      glReadPixels(0, 0, data.width, data.height, GL_RGB, GL_UNSIGNED_BYTE, data.buffer);
      fwrite(data.buffer, data.bufsize, 1, stdout);
    }
  }
  glutReportErrors();
}

void cleanup(void) {
  if (data.buffer) { free(data.buffer); }
  if (data.eca) { pclose(data.eca); }
}

int main(int argc, char **argv) {
  data.width = 1050;
  data.height = 576;
  data.frames = 4425;
  data.record = 0;
  mix(&red, &black, 1.0/3.0, &darkRed);
  mix(&blue, &black, 2.0/3.0, &darkBlue);
  glutInitWindowSize(data.width, data.height);
  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
  glutInit(&argc, argv);
  glutCreateWindow("Incidents At Sea");
  glewInit();
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  boat_init(&data.boat);
  wave_init(&data.wave);
  if (data.record) {
    data.bufsize = data.width * data.height * 3;
    data.buffer = malloc(data.bufsize);
    memset(data.buffer, 128, data.bufsize);
  } else {
    data.buffer = 0;
  }
  data.frame = 0;
  data.ready = 0;
  glClearColor(0.5, 1.0, 0.5, 1);
  glutReshapeFunc(reshape);
  glutDisplayFunc(display);
  glutTimerFunc(1, timer, 0);
  glutKeyboardFunc(keyboard);
  glutReportErrors();
  data.jack = jack_client_open("incidents", 0, 0);
  data.eca = popen("ecasound -G:jack,incidents,send -i:../audio/nightboat.wav -o:jack,system", "w");
  atexit(cleanup);
  glutMainLoop();
  return 0;
}
