/*

    stroking a path with a constant perpendicular width

    approximate sinusoid at P by a straight line tangent

              P     drop a vertical line from P to Q
             /|     PRO  is a right angle
           O/_|R    POQ  is a right angle
           /\ |     |RO| is one unit
          /  \|     |OQ| is 'width'
         /    Q     PRO  is similar to POQ

    |PQ|:|OQ| = |PO|:|RO| -> |PQ| = width * sqrt(1^2 + dy^2)

    gnuplot example

        set size square
        set xrange [-pi:pi]
        set yrange [-pi:pi]
        plot sin(x),sin(x)+0.5*sqrt(1+cos(x)*cos(x)),sin(x)-0.5*sqrt(1+cos(x)*cos(x))

*/

/* wave shape */
uniform float amplitude;
uniform float frequency;
uniform float level;
uniform float phase;

/* appearance */
uniform float width;
uniform float blend;
uniform vec4 stroke;
uniform vec4 fill;

const vec4 transparent = vec4(0.0);

void main(void) {
  vec2 p = gl_TexCoord[0].xy;
  float t = frequency * p.x + phase;
  float y = amplitude * sin(t) + level;
  float dy = amplitude * cos(t) * frequency;
  float delta = width * sqrt(1.0 + dy * dy);
  float y0 = y - delta;
  float y1 = y + delta;
  vec4 c;
  if (p.y < y0) {
    c = mix(fill, stroke, clamp(1.0 - blend * (y0 - p.y), 0.0, 1.0));
  } else if (p.y < y1) {
    c = stroke;
  } else {
    float a = clamp(blend * (p.y - y1), 0.0, 1.0);
    c = vec4(stroke.rgb, 1.0 - a);
  }
  gl_FragColor = c;
}
