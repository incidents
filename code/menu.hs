{-# LANGUAGE NoMonomorphismRestriction #-}

import Control.Monad (forM_, when)
import Graphics.Rendering.Cairo

import Font
import Colour
import Track

width  = 1536
height = 8192

menu = do
  setSourceRGBA 0 0 0 0
  paint
  setLineCap LineCapRound
  setLineJoin LineJoinRound
  save
  scale (width / 3) (width / 3)
  translate (0.5) (-0.5)
  forM_ (titles `zip` [0..]) $ \(track, number) -> do
    save
    translate 1 (1 + fromIntegral number)
    let angle = atan2 1 3
    rotate $ if even number then angle else -angle
    scale 3 3
    translate (-0.5) (-0.5)
    string $ " " ++ track ++ " "
    restore
  restore

main = do
  image <- createImageSurface FormatARGB32 width height
  renderWith image menu
  surfaceWriteToPNG image "menu.png"
