#include "vector.h"

void mix(const struct vec4 *x, const struct vec4 *y, float t, struct vec4 *o) {
  float s = 1 - t;
  o->v[0] = x->v[0] * s + t * y->v[0];
  o->v[1] = x->v[1] * s + t * y->v[1];
  o->v[2] = x->v[2] * s + t * y->v[2];
  o->v[3] = x->v[3] * s + t * y->v[3];
}
