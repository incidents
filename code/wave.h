#ifndef WAVE_H
#define WAVE_H 1

#include "shader.h"
#include "vector.h"

struct wave { struct shader shader;
  struct { GLint amplitude, frequency, level, phase, width, blend,             stroke, fill; } uniform;
  struct { float amplitude, frequency, level, phase, width, blend; struct vec4 stroke, fill; } value;
};

struct wave *wave_init(struct wave *wave);
void wave_use(struct wave *wave);

#endif
