module Font (glyph, string) where

import Control.Monad (forM_)
import Graphics.Rendering.Cairo

-- renders a character filling a 2x2 box
glyph :: Char -> Render ()
glyph 'a' = moveTo 0 2 >> lineTo 0 1 >> arc 1 1 1 pi 0 >> lineTo 2 2 >> moveTo 0 1 >> lineTo 2 1
glyph 'b' = moveTo 0 1 >> lineTo 1.5 1 >> arcNegative 1.5 0.5 0.5 (pi/2) (-pi/2) >> lineTo 0 0 >> lineTo 0 2 >> lineTo 1.5 2 >> arcNegative 1.5 1.5 0.5 (pi/2) (-pi/2)
glyph 'c' = moveTo 2 0 >> lineTo 1 0 >> arcNegative 1 1 1 (-pi/2) (pi/2) >> lineTo 2 2
glyph 'd' = moveTo 1 0 >> lineTo 0 0 >> lineTo 0 2 >> lineTo 1 2 >> arcNegative 1 1 1 (pi/2) (-pi/2)
glyph 'e' = moveTo 2 0 >> lineTo 0 0 >> lineTo 0 2 >> lineTo 2 2 >> moveTo 0 1 >> lineTo 1 1
glyph 'f' = moveTo 2 0 >> lineTo 0 0 >> lineTo 0 2 >> moveTo 0 1 >> lineTo 1 1
glyph 'g' = moveTo 2 0 >> lineTo 1 0 >> arcNegative 1 1 1 (-pi/2) 0 >> lineTo 1 1
glyph 'h' = moveTo 0 0 >> lineTo 0 2 >> moveTo 0 1 >> lineTo 2 1 >> moveTo 2 0 >> lineTo 2 2
glyph 'i' = moveTo 0 0 >> lineTo 2 0 >> moveTo 1 0 >> lineTo 1 2 >> moveTo 0 2 >> lineTo 2 2
glyph 'j' = moveTo 0 0 >> lineTo 2 0 >> moveTo 0 2 >> arcNegative 0 1 1 (pi/2) 0 >> lineTo 1 0
glyph 'k' = moveTo 0 0 >> lineTo 0 2 >> moveTo 0 1 >> lineTo 1 1 >> lineTo 2 0 >> moveTo 1 1 >> lineTo 2 2
glyph 'l' = moveTo 0 0 >> lineTo 0 2 >> lineTo 2 2
glyph 'm' = moveTo 0 2 >> lineTo 0 0 >> lineTo 1 1 >> lineTo 2 0 >> lineTo 2 2
glyph 'n' = moveTo 0 2 >> lineTo 0 0 >> lineTo 2 2 >> lineTo 2 0
glyph 'o' = moveTo 1 0 >> arc 1 1 1 (-pi/2) (pi/2) >> arc 1 1 1 (pi/2) (-pi/2)
glyph 'p' = moveTo 0 1 >> lineTo 1.5 1 >> arcNegative 1.5 0.5 0.5 (pi/2) (-pi/2) >> lineTo 0 0 >> lineTo 0 2
glyph 'q' = moveTo 1 0 >> arc 1 1 1 (-pi/2) (pi/2) >> arc 1 1 1 (pi/2) (-pi/2) >> moveTo 1 1 >> lineTo 2 2
glyph 'r' = moveTo 0 1 >> lineTo 1.5 1 >> arcNegative 1.5 0.5 0.5 (pi/2) (-pi/2) >> lineTo 0 0 >> lineTo 0 2 >> moveTo 1 1 >> lineTo 2 2
glyph 's' = moveTo 2 0 >> lineTo 0.5 0 >> arcNegative 0.5 0.5 0.5 (-pi/2) (pi/2) >> lineTo 1.5 1 >> arc 1.5 1.5 0.5 (-pi/2) (pi/2) >> lineTo 0 2
glyph 't' = moveTo 0 0 >> lineTo 2 0 >> moveTo 1 0 >> lineTo 1 2
glyph 'u' = moveTo 0 0 >> lineTo 0 1 >> arcNegative 1 1 1 pi 0 >> lineTo 2 0
glyph 'v' = moveTo 0 0 >> lineTo 0 1 >> lineTo 1 2 >> lineTo 2 1 >> lineTo 2 0
glyph 'w' = moveTo 0 0 >> lineTo 0 2 >> lineTo 1 1 >> lineTo 2 2 >> lineTo 2 0
glyph 'x' = moveTo 0 0 >> lineTo 2 2 >> moveTo 2 0 >> lineTo 0 2
glyph 'y' = moveTo 0 0 >> lineTo 1 1 >> lineTo 2 0 >> moveTo 1 1 >> lineTo 1 2
glyph 'z' = moveTo 0 0 >> lineTo 2 0 >> lineTo 0 2 >> lineTo 2 2
glyph _ = return ()

-- renders a string centered vertically filling horizontally a 1x1 box
string :: String -> Render ()
string str = do
  save
  let n = fromIntegral . length $ str
      s = 0.5 / n
  scale s s
  translate 1 n
  forM_ str $ \c -> save >> scale (sqrt 0.5) (sqrt 0.5) >> translate (-1) (-1) >> glyph c >> restore >> translate 2 0
  setSourceRGB 0 0 0
  setLineWidth 0.5
  strokePreserve
  setSourceRGB 1 1 1
  setLineWidth 0.25
  stroke
  restore
