{-# LANGUAGE NoMonomorphismRestriction #-}

import Control.Monad (forM_, when)
import Data.List (zip4)
import Graphics.Rendering.Cairo

width  = 2 * pi * height
height = 576

red    = map (/255) [231,  13, 100]
orange = map (/255) [255, 200, 103]
green  = map (/255) [153, 255,  53]
blue   = map (/255) [  3,  71, 148]
black  = map (/255) [  0,   0,   0]

blend k = zipWith (\a b -> a * (1 - k) + k * b)

tracks =
  [ "nightboat"
  , "semaphore"
  , "prepare to be boarded"
  , "no quarter"
  , "sonar"
  , "subterfuge"
  , "bulkhead collapse"
  , "man overboard"
  , "inflatable"
  , "aeronautics"
  , "engine failure"
  , "freefall"
  , "cloctopus"
  , "fisheye"
  , "coda"
  , "bubblicious"
  ]

durations = [ 4390, 3092, 1978, 935, 1298, 8200, 2965, 4995, 3965, 5213, 3996, 4765, 6113, 7892, 4318, 2667 ]
duration = sum durations

(trackWidths, trackHeights) = unzip $ zipWith f tracks durations
  where
    f name dur =
      let area = aspect^3 -- recip $ dur / duration
          aspect = fromIntegral (length name)
          -- area = w * h, w = aspect h -> area = aspect (h^2) -> h = sqrt (area / aspect)
      in (sqrt (aspect * area), sqrt (area / aspect))

horizontal = sum . map fst . filter (even . snd) . (`zip` [0..]) $ trackWidths
vertical   = sum . map fst . filter (odd  . snd) . (`zip` [0..]) $ trackWidths
incline = atan2 vertical' horizontal'

horizontal' = sum . map fst . filter (even . snd) . (`zip` [0..]) $ trackHeights
vertical'   = sum . map fst . filter (odd  . snd) . (`zip` [0..]) $ trackHeights
diagonal = sqrt $ horizontal'^2 + vertical'^2

string xs = do
  save
  let n = fromIntegral . length $ xs
      s = 0.5 / n
  scale s s
  translate 1 n
  forM_ xs $ \c -> save >> scale (sqrt 0.5) (sqrt 0.5) >> translate (-1) (-1) >> glyph c >> restore >> translate 2 0
  setSourceRGB 0 0 0
  setLineWidth 0.5
  strokePreserve
  setSourceRGB 1 1 1
  setLineWidth 0.25
  stroke
  restore

menu = do
  setSourceRGBA 0 0 0 0
  paint
  setLineCap LineCapRound
  setLineJoin LineJoinRound
  save
  translate 0 (height * 3 / 4)
  let s = width / diagonal
  scale s s
  rotate (-incline)
  forM_ (zip4 [0..] tracks trackWidths trackHeights) $ \(number, track, w, h) -> do
    save
    scale h h
    rotate $ if even number then 0 else pi/2
    translate 0 (-0.5)
    string $ " " ++ track ++ " "
    restore
    if even number then translate h 0 else translate 0 h
  restore

main = do
  image <- createImageSurface FormatARGB32 (round width) height
  renderWith image menu
  surfaceWriteToPNG image "menu.png"


glyph 'a' = moveTo 0 2 >> lineTo 0 1 >> arc 1 1 1 pi 0 >> lineTo 2 2 >> moveTo 0 1 >> lineTo 2 1
glyph 'b' = moveTo 0 1 >> lineTo 1.5 1 >> arcNegative 1.5 0.5 0.5 (pi/2) (-pi/2) >> lineTo 0 0 >> lineTo 0 2 >> lineTo 1.5 2 >> arcNegative 1.5 1.5 0.5 (pi/2) (-pi/2)
glyph 'c' = moveTo 2 0 >> lineTo 1 0 >> arcNegative 1 1 1 (-pi/2) (pi/2) >> lineTo 2 2
glyph 'd' = moveTo 1 0 >> lineTo 0 0 >> lineTo 0 2 >> lineTo 1 2 >> arcNegative 1 1 1 (pi/2) (-pi/2)
glyph 'e' = moveTo 2 0 >> lineTo 0 0 >> lineTo 0 2 >> lineTo 2 2 >> moveTo 0 1 >> lineTo 1 1
glyph 'f' = moveTo 2 0 >> lineTo 0 0 >> lineTo 0 2 >> moveTo 0 1 >> lineTo 1 1
glyph 'g' = moveTo 2 0 >> lineTo 1 0 >> arcNegative 1 1 1 (-pi/2) 0 >> lineTo 1 1
glyph 'h' = moveTo 0 0 >> lineTo 0 2 >> moveTo 0 1 >> lineTo 2 1 >> moveTo 2 0 >> lineTo 2 2
glyph 'i' = moveTo 0 0 >> lineTo 2 0 >> moveTo 1 0 >> lineTo 1 2 >> moveTo 0 2 >> lineTo 2 2
glyph 'j' = moveTo 0 0 >> lineTo 2 0 >> moveTo 0 2 >> arcNegative 0 1 1 (pi/2) 0 >> lineTo 1 0
glyph 'k' = moveTo 0 0 >> lineTo 0 2 >> moveTo 0 1 >> lineTo 1 1 >> lineTo 2 0 >> moveTo 1 1 >> lineTo 2 2
glyph 'l' = moveTo 0 0 >> lineTo 0 2 >> lineTo 2 2
glyph 'm' = moveTo 0 2 >> lineTo 0 0 >> lineTo 1 1 >> lineTo 2 0 >> lineTo 2 2
glyph 'n' = moveTo 0 2 >> lineTo 0 0 >> lineTo 2 2 >> lineTo 2 0
glyph 'o' = moveTo 1 0 >> arc 1 1 1 (-pi/2) (pi/2) >> arc 1 1 1 (pi/2) (-pi/2)
glyph 'p' = moveTo 0 1 >> lineTo 1.5 1 >> arcNegative 1.5 0.5 0.5 (pi/2) (-pi/2) >> lineTo 0 0 >> lineTo 0 2
glyph 'q' = moveTo 1 0 >> arc 1 1 1 (-pi/2) (pi/2) >> arc 1 1 1 (pi/2) (-pi/2) >> moveTo 1 1 >> lineTo 2 2
glyph 'r' = moveTo 0 1 >> lineTo 1.5 1 >> arcNegative 1.5 0.5 0.5 (pi/2) (-pi/2) >> lineTo 0 0 >> lineTo 0 2 >> moveTo 1 1 >> lineTo 2 2
glyph 's' = moveTo 2 0 >> lineTo 0.5 0 >> arcNegative 0.5 0.5 0.5 (-pi/2) (pi/2) >> lineTo 1.5 1 >> arc 1.5 1.5 0.5 (-pi/2) (pi/2) >> lineTo 0 2
glyph 't' = moveTo 0 0 >> lineTo 2 0 >> moveTo 1 0 >> lineTo 1 2
glyph 'u' = moveTo 0 0 >> lineTo 0 1 >> arcNegative 1 1 1 pi 0 >> lineTo 2 0
glyph 'v' = moveTo 0 0 >> lineTo 0 1 >> lineTo 1 2 >> lineTo 2 1 >> lineTo 2 0
glyph 'w' = moveTo 0 0 >> lineTo 0 2 >> lineTo 1 1 >> lineTo 2 2 >> lineTo 2 0
glyph 'x' = moveTo 0 0 >> lineTo 2 2 >> moveTo 2 0 >> lineTo 0 2
glyph 'y' = moveTo 0 0 >> lineTo 1 1 >> lineTo 2 0 >> moveTo 1 1 >> lineTo 1 2
glyph 'z' = moveTo 0 0 >> lineTo 2 0 >> lineTo 0 2 >> lineTo 2 2
glyph _ = return ()
